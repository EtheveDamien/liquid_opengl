local SimplePhysicMetaBall = require("objects/SimplePhysicMetaBall")

local MetaBallManager = {}
MetaBallManager.__index = MetaBallManager

function MetaBallManager:new()
  local self = {
    metaBalls = {},
    pixelcode = [[ 
      vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
      {
        vec4 col = Texel(texture, texture_coords);

        if(col.a*color.a == 0)
          return col;
      
        if(col.a > 0.99)
          return vec4(1, 1, 1, col.a)*color;
        else
          return vec4(0, 0, 0, 0);
      }
    ]]
  }
  self.shader = love.graphics.newShader(self.pixelcode)

  setmetatable(self, MetaBallManager)
  return self
end

function MetaBallManager:addMetaBall(px, py)
  local metaBall = SimplePhysicMetaBall:new(
    px, py,
    math.random( -30, 30 ), 0
  )

  table.insert(self.metaBalls, metaBall)
end

function MetaBallManager:update(dt)
  if #self.metaBalls == 200 then
    table.remove( self.metaBalls, 1)
  end

  for i,v in ipairs(self.metaBalls) do
    v:update(dt)
  end
end

function MetaBallManager:draw(canvas)
  love.graphics.setCanvas(canvas)
  love.graphics.clear(0,0,0,0)

  for i,v in ipairs(self.metaBalls) do
    v:draw()
  end

  love.graphics.setCanvas()
  love.graphics.setShader(self.shader)
  love.graphics.setColor(0,0,0,1)
  love.graphics.draw(canvas)
  love.graphics.setShader()
end

return MetaBallManager
