local MetaBall = {}
MetaBall.__index = MetaBall

function MetaBall:new(pX, pY, imageLocation)
  local self = {}
  self.x = pX
  self.y = pY
  self.img = love.graphics.newImage(imageLocation)

  setmetatable(self, MetaBall)
  return self
end

function MetaBall:draw()
  love.graphics.draw(
    self.img, 
    self.x,
    self.y,
    0, 1,1,
    self.img:getWidth()/2,
    self.img:getHeight()/2
  )
end

return MetaBall
